imports.gi.versions.Gtk = '3.0';
imports.gi.versions.Gdk = '3.0';
const {GObject, Gdk, Gio, Gtk } = imports.gi;

const file = Gio.File.new_for_path('devicefinder.ui');
const [, template] = file.load_contents(null);

var DeviceFinder = GObject.registerClass({
	GTypeName: 'DeviceFinder',
    //Template: 'url://templateurl',
    Template: template,
	Children: ['main_box'],
	InternalChildren: [
		'device_list',
		'device_list_button',
		'device_info_button',
		'device_info_label',
		'device_data_button',
		'device_data_scroller',
		'device_data_label',
		'clear_device_data_button'
	]
}, class DeviceFinder extends Gtk.Window {
	/* implementation */
	_init(params = {}) {
		super._init(params);

		/*	The template has been initialized and you can access the children
		*/
		this.main_box.visible = true;

		/*	Internal children are set on the instance prefixed with a `_`
		*	e.g.
		*		this._device_list_button.visible = true;
		*/

		this.selected_device = {};
		this.data_label_line_limit = 2000;

	}

	/*	This is our access to udpnet instance
	*/
	set_udp_instance(udpnet) {
		this.udpnet = udpnet;
	}

	/*	The signal handlers bound in the UI file
	*/
	on_device_list_button_clicked(button) {
		//	Clear existing list
		this._device_list.get_children().forEach( (child) => {
			this._device_list.remove(child);
		});

		//	Request new list details
		this.udpnet.refresh_device_lists();
	}
	on_device_info_button_clicked(button) {
		if (this instanceof Gtk.Window)
			log('Callback scope is bound to `DeviceFinder`');
	}
	on_device_data_button_clicked(button) {
		if (this instanceof Gtk.Window)
			log('Callback scope is bound to `DeviceFinder`');
	}
	on_clear_device_data_button_clicked(button) {
		this.set_data_text("");
	}
	on_main_window_destroy(button) {
		Gtk.main_quit();
	}
	on_DeviceFinder_show(obj) {
		log('Active!');
	}

	/*	Other functions
	*/
	get_info_text() {
		return this._device_info_label.get_text();
	}
	set_info_text(new_text) {
		this._device_info_label.set_text(new_text);
	}

	get_data_text() {
		return this._device_data_label.get_text();
	}
	set_data_text(new_text) {
		this._device_data_label.set_text(new_text);
	}

	/*	attached_devices()
	*
	*	Process a new list of devices from a particular server
	*	i.e. populate device listbox display.
	*/
	attached_devices(server, devices) {
		/*
		*	If we have existing entries from this server, remove them
		*	since we're updating with a new list of devices from that server.
		*/
		this._device_list.foreach( (child) => {
			if (child.get_children()[0].get_children()[0].get_children()[1].get_text() == server) {
				log(`removing sid : ${server}`);
				this._device_list.remove(child);
			}
		});

		if (devices.length < 1) {
			console.log(`attached_devices 0`);
			return;	//Nothing to do
		}

		let leader;
		if (this._device_list.get_children().length > 0) {
			leader = this._device_list.get_children()[0].get_children()[0];
		}
		devices.forEach( (device) => {
			var row = new Gtk.ListBoxRow();

			if (leader) {
				var description_button = Gtk.RadioButton.new_from_widget(leader);
			} else {
				var description_button = new Gtk.RadioButton();
				leader = description_button;
			}

			/*	Each radio button will have single child box with three compartment children.
			*	Each compartment child contains the label to display.
			*	The server_id is in second compartment, so access its text with:
			*		entry.get_children()[0].get_children()[1].get_text();
			*	The device-triple is in third compartment, so access its text with:
			*		entry.get_children()[0].get_children()[2].get_text();
			*
			*	Having extracted the server_id & device triple,
			*	obtain all the device's info and display it in the ui.
			*/
			description_button.connect("pressed", (entry) => {
				//	Extract the device-triple, as well as the server it's attached to
				const server_id = entry.get_children()[0].get_children()[1].get_text();
				const triple = entry.get_children()[0].get_children()[2].get_text();
				this.selected_device["server_id"] = server_id;
				this.selected_device["triple"] = triple;

				//	Obtain all the device info
				const some_device_data = this.udpnet.find_device_data(triple, server_id);
				//	Display the device info
				this.set_info_text(JSON.stringify(some_device_data, null, 2));
			});

			var hbox = new Gtk.Box({orientation:Gtk.Orientation.HORIZONTAL});
			const name_label = Gtk.Label.new(device.name);
				name_label.set_width_chars(28);
				name_label.set_xalign(0);
				hbox.pack_start(name_label, true, false, 0);
			const server_label = Gtk.Label.new(server);
				server_label.set_width_chars(24);
				server_label.set_xalign(0);
				hbox.pack_start(server_label, true, false, 0);
			const triple_label = Gtk.Label.new(device.temp_id);
				triple_label.set_width_chars(12);
				triple_label.set_xalign(0);
				hbox.pack_start(triple_label, true, false, 0);
			description_button.add(hbox);

			row.add(description_button);
			row.className = "device_entries";
			this._device_list.add(row);
		});
		this._device_list.show_all();
		//	Try to select the same device that was selected prior to update
		if (this.selected_device.hasOwnProperty("server_id")) {
			//log(`Restore selection of ${this.selected_device.triple} at ${this.selected_device.server_id}`);
			this._device_list.get_children().forEach( (child) => {
				const server_id = child.get_children()[0].get_children()[0].get_children()[1].get_text();
				const triple = child.get_children()[0].get_children()[0].get_children()[2].get_text();
				if (server_id == this.selected_device.server_id && triple == this.selected_device.triple) {
					child.get_children()[0].set_active(true);
				}
			});
		} else {
			//	Nothing selected - assume first should be selected
			const server_id = this._device_list.get_children()[0].get_children()[0].get_children()[0].get_children()[1].get_text();
			const triple = this._device_list.get_children()[0].get_children()[0].get_children()[0].get_children()[2].get_text();
			this.selected_device["server_id"] = server_id;
			this.selected_device["triple"] = triple;

			//	Obtain all the device info
			const some_device_data = this.udpnet.find_device_data(triple, server_id);
			//	Display the device info
			this.set_info_text(JSON.stringify(some_device_data, null, 2));
		}
	}

	/*	Display a message in the device data label
	*
	*
	*/
	device_event(message, rinfo) {
		const regexp = /\n/g;
		var data_label_line_count = [...this._device_data_label.get_text().matchAll(regexp)].length;

		if (this._device_data_label.get_text().length == 0) {
			this._device_data_label.set_text(this._device_data_label.get_text() + message);
		} else if (data_label_line_count > this.data_label_line_limit) {
			//	Truncate the first 10 lines, leaving most recent data
			this._device_data_label.set_text(this._device_data_label.get_text().replace(/(.*\s){10}/,"") + "\n" + message);
		} else {
			this._device_data_label.set_text(this._device_data_label.get_text() + "\n" + message);
		}
		//	Scroll to end
		var adjustment = this._device_data_scroller.get_vadjustment();
		adjustment.set_value(adjustment.get_upper() - adjustment.get_page_size());
	}

});

