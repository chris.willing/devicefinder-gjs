# devicefinder-gjs

**_devicefinder-gjs_** is designed to monitor and manage Xkeys devices on the local network.

Gnome JavaScript (gjs) is used to implement an Xkeys device finder & management tool using the new (2022) UDP based Xkeys **Dynamic Control Data** protocol (DCD) to communicate with multiple [Xkeys Servers](https://gitlab.com/chris.willing/xkeys-server) (either software or hardware based).

The current development status is illustrated in the following screen shot. It shows multiple Xkeys devices which have been discovered attached to two different servers. When a particular device is selected in the device list panel, technical information about the device is displayed in the device info panel. The device data panel shows a stream of live events from all of the listed devices.

Note that two _XK-24_ devices have been discovered, one attached to _XKS_freddy_, the other to _XKS-pi4b_.
<p align="center" width="100%" ><img width="70%" src="assets/devicefinder.png" ></p>
Further development will now concentrate on sending commands to the various devices.


## Installation
[Gnome Javascript](https://gitlab.gnome.org/GNOME/gjs) is required to be installed to run **_devicefinder-gjs_**. It should already be installed in most Linux distributions or available from the distribution package manager.

During active development, **_devicefinder-gjs_** is most easily installed by cloning this repository. Running the _main.js_ app in the resulting directory will activate **_devicefinder-gjs_**. A typical sequence of terminal commands would be:
```
cd
git clone https://gitlab.com/chris.willing/devicefinder-gjs
cd devicefinder-gjs
./main.js
```

## Usage

## Support
Questions regarding development or usage are best addressed by [raising an issue here](https://gitlab.com/chris.willing/devicefinder-gjs/-/issues).

## Contributing
[Merge requests are welcome here](https://gitlab.com/chris.willing/devicefinder-gjs/-/merge_requests) but it is suggested that ideas for development or changes are first discussed by [raising an issue](https://gitlab.com/chris.willing/devicefinder-gjs/-/issues).

## Acknowledgments
This project has been made possible by generous funding and donation of Xkeys devices by [P.I. Engineering](https://xkeys.com/)

## License
**_devicefinder-gjs_** is published initially under the terms of the MIT license (see LICENSE file) or, at the choice of the user, under the terms of LGPL-2.0-or-later.

